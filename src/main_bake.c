#include <bake.h>
#include <bake_mcpp.h>

#include "mcpp_lib.h"

static
void generate(
    bake_driver_api *driver,
    bake_config *config,
    bake_project *project_obj)
{
    bake_attr *args_attr = driver->get_attr("args");
    if (args_attr) {
        int    argi = 0;
        int    argc = args_attr->is.array->size;
        char** argv = malloc(sizeof(char*) * argc);

        ut_iter it = ut_ll_iter(args_attr->is.array);
        while (ut_iter_hasNext(&it)) {
            bake_attr *el = ut_iter_next(&it);

            argv[argi++] = ut_envparse(el->is.string);
        }

        mcpp_reset_def_out_func();
        mcpp_lib_main(argc, argv);

        free(argv);
    }

}

BAKE_MCPP_EXPORT 
int bakemain(bake_driver_api *driver) {
    driver->generate(generate);
    return 0;
}