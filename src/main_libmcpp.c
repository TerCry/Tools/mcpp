/* most simple sample source to use libmcpp */
#ifndef __BAKE__

#include "mcpp_lib.h"

int
main (int argc, char *argv[])
{
    return mcpp_lib_main (argc, argv);
}

#endif